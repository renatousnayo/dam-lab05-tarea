import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  TouchableOpacity,
  Button,
  FlatList,
  Alert,
} from 'react-native';

import ConexionFetch from './app/components/conexionFetch/ConexionFetch';

import Message from './app/components/message/Message';

import Body from './app/components/body/Body';

import OurFlatList from './app/components/ourFlatList/OurFlatList';


import {NavigationContainer} from '@react-navigation/native';

import {createStackNavigator} from '@react-navigation/stack';


function Login({navigation}) {
  return (
    <View style={{flex: 1, backgroundColor: '#212121', alignItems: 'center', justifyContent: 'center'}}>
      <Text style={{fontSize: 30,color: 'white'}}>Iniciar Sesion</Text>
      <Image style={{height: 150, width:150, alignSelf: 'center'}} source={{uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTVXn0HEgD2bRe8nMGN8owyVCyt_XVN_jogEHYLNG5-R-0_zyvp&usqp=CAU'}} />

      <View >
          <Text style={{fontSize: 25, color: 'white'}}> Usuario </Text>
        </View>

      <TextInput
          style={{height: 40, width: 150, borderColor: 'gray', borderWidth: 1}}
          placeholder="Usuario"
          />

      <View >
          <Text style={{fontSize: 25, color: 'white'}}> Contraseña </Text>
        </View>  

      <TextInput
          style={{height: 40, width: 150, borderColor: 'gray', borderWidth: 1}}
          placeholder="Contraseña"
          />

      <Button
        title="Ingresar"
        onPress={() => navigation.navigate('List')}
      />
    </View>
  );
}

function Lista({navigation}) {
  return (
    <OurFlatList navigation={navigation}/>
  );
}

function DetailsScreen({route, navigation}) {

  const {itemTitle} = route.params;
  const {itemID} = route.params;
  const {itemDesc} = route.params;
  const {itemPic} = route.params;

  return (
    <View style={{backgroundColor: '#212121', flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      
        <Text style={{color: 'white' , fontSize: 40, marginBottom: 20}}>{itemTitle}</Text>
        <Image style={{aspectRatio: 2/3 , width:250 , marginBottom: 20} } source={{uri: itemPic}} />
        <Text style={{color: 'white' , fontSize: 24, marginBottom: 20}}>Descripcion: {itemDesc}</Text>

      <TouchableOpacity onPress = {() => navigation.navigate('List')}>
        <View style = {{padding:10, backgroundColor: '#2196f3', alignItems: 'center', justifyContent: 'center', borderRadius: 4}}>
          <Text style = {{fontSize: 20, color: 'white'}}>VOLVER</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}

const Stack = createStackNavigator();


export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      textValue: 0,
      count: 0,
      items: [],
      error: null,
    };
  }

  componentDidMount() {
    console.warn('Estamos dentro componentDidMount', this.state.textValue);
  }

  shouldComponentUpdate() {
    console.warn('Estamos dentro shouldComponentUpdate', this.state.error);
  }

  changeTextInput = text => {
    this.setState({textValue: text});
  };

  onPress = () => {
    this.setState({
      count: this.state.count + 1,
    });
  };

  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Log" component={Login}/> 
          <Stack.Screen name="List" component={Lista}/>
          <Stack.Screen name="Details" component={DetailsScreen} />

        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

const styles = StyleSheet.create({
  text: {
    alignItems: 'center',
    
    fontSize: 60,
    padding: 10,
  },

  button: {
    top: 10,
    fontSize: 50,
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
  },
  countContainer: {
    alignItems: 'center',
    padding: 10,
  },
  countText: {
    color: '#FF00FF',
  },

  container: {
    flex: 1,
    marginTop: 20,
  },
  item: {
    backgroundColor: 'orange',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
});
